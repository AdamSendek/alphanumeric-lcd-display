# Alphanumeric Lcd Display
Last update: 2024

## Description
Driver to alphanumeric lcd display

## Hardware
* Microcontroller STM32
* Alphanumeric LCD Display

### 3D Print
...  

## Software
VS Code
C   11

## Usage
...  

## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0

/*
 * Copyright (C) 2024 Adam Sendek
 *
 * These computer program listings and specifications,
 * are property of Adam Sendek and are licensed under
 * Creative Commons Attribution-NonCommercial-ShareAlike
 * 4.0 International Public License
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "alphanumeric_Display.h"



#define MAX_SUPPORT_ROW             4
#define MAX_SUPPORT_COLUMN          20
#define BACKLIGHT_LIGHT_PRECISION   256
#define MAX_12BIT                   4095

#define CMD_CLEAR                   0x01
#define CMD_RETURN_HOME             0x02
#define CMD_CURSOR_MODE_SET         0x04
#define CMD_DISPLAY_BEHAVIOR        0x08
#define CMD_SHIFT_CURSOR            0x10
#define CMD_CONFIGURATION_SET       0x20
#define CMD_CHAR_GENERATOR_ADDRESS  0x40
#define CMD_DISPLAY_RAM_ADDRESS     0x80

// CMD_CURSOR_MODE_SET
#define CURSOR_BLINK_ON             1
#define CURSOR_BLINK_OFF            0
#define CURSOR_ON                   1 << 1
#define CURSOR_OFF                  0 << 1
#define DISPLAY_ON                  1 << 2
#define DISPLAY_OFF                 0 << 2

// CMD_DISPLAY_BEHAVIOR
#define BEHAVIOR_CURSOR_SHIFT       0
#define BEHAVIOR_DATA_SHIFT         1
#define BEHAVIOR_SHIFT_LEFT         0 << 1
#define BEHAVIOR_SHIFT_RIGHT        1 << 1

// CMD_SHIFT_CURSOR
#define CURSOR_SHIFT                0 << 2
#define DATA_SHIFT                  1 << 2
#define SHIFT_LEFT                  0 << 3
#define SHIFT_RIGHT                 1 << 3

// CMD_CONFIGURATION_SET
#define CHAR_MATRIX_5X10            1 << 2
#define CHAR_MATRIX_5X7             0 << 2
#define DISPLAY_TWO_LINE            1 << 3
#define DISPLAY_ONE_LINE            0 << 3
#define INTERFACE_4BIT              1 << 4
#define INTERFACE_8BIT              0 << 4



enum dataType {  // RS pin
    COMMAND = GPIO_PIN_RESET,
    DATA = GPIO_PIN_SET,
};

enum backlightMode {
    BACKLIGHT_OFF = 0,
    BACKLIGHT_IO,
    BACKLIGHT_PWM,
};

struct alphanumericDisplayConfig {
    uint32_t pinPower;
    GPIO_TypeDef *portPower;
    DAC_HandleTypeDef *dacV0;
    uint32_t dacChannelV0;
    uint32_t pinRS;
    GPIO_TypeDef *portRS;
    uint32_t pinRW;
    GPIO_TypeDef *portRW;
    uint32_t pinE;
    GPIO_TypeDef *portE;
    uint32_t pinD0;
    GPIO_TypeDef *portD0;
    uint32_t pinD1;
    GPIO_TypeDef *portD1;
    uint32_t pinD2;
    GPIO_TypeDef *portD2;
    uint32_t pinD3;
    GPIO_TypeDef *portD3;
    uint32_t pinD4;
    GPIO_TypeDef *portD4;
    uint32_t pinD5;
    GPIO_TypeDef *portD5;
    uint32_t pinD6;
    GPIO_TypeDef *portD6;
    uint32_t pinD7;
    GPIO_TypeDef *portD7;
    uint32_t pinBacklight;
    GPIO_TypeDef *portBacklight;
    TIM_HandleTypeDef *timBacklight;
    uint32_t timChannelBacklight;
    bool mode8bit;  // 8bit or 4bit mode
    bool modeRwOff;
    bool modeControlPower;
    bool modeControlContrast;
    enum backlightMode modeBacklight;
    uint8_t row;
    uint8_t column;
    uint8_t rowOffsets[4];
};

static struct alphanumericDisplayConfig adconf {
    .mode8bit = false,
    .modeRwOff = false,
    .modeControlPower = false,
    .modeControlContrast = false,
    .backlightMode modeBacklight = BACKLIGHT_OFF,
    .row = 2;
    .column = 16;
};

static void updateDisplay(void) {
    HAL_GPIO_WritePin(adconf.portE, adconf.pinE, GPIO_PIN_RESET);
    HAL_Delay(1);
    HAL_GPIO_WritePin(adconf.portE, adconf.pinE, GPIO_PIN_SET);
#ifdef(DELAY_US_H_)
    delay_us(1);  // >450 ns
#else
    HAL_Delay(1);
#endif
    HAL_GPIO_WritePin(adconf.portE, adconf.pinE, GPIO_PIN_RESET);
#ifdef(DELAY_US_H_)
    delay_us(40);  // >37 us
#else
    HAL_Delay(1);
#endif
}

static void write(uint8_t val, enum dataType dt) {
    HAL_GPIO_WritePin(adconf.portRS, adconf.pinRS, dt);

    if (adconf.mode8bit) {
        for (int i = 0; i < 8; i++) {
            HAL_GPIO_WritePin(adconf.portD[i], adconf.pinD[i], val & (1 << i));
        }
        updateDisplay();
    } else {
        for (int i = 4; i < 8; i++) {
            HAL_GPIO_WritePin(adconf.portD[i], adconf.pinD[i], val & (0b10000 << i));
        }
        updateDisplay();
        for (int i = 4; i < 8; i++) {
            HAL_GPIO_WritePin(adconf.portD[i], adconf.pinD[i], val & (1 << i));
        }
        updateDisplay();
    }
}

/**
 * @brief Clean all screen
 *
 */
void alphanumericDisplay_clear(void) {
    write(CMD_CLEAR, COMMAND);
    HAL_Delay(2);  // 2ms
}

/**
 * @brief Shift cursor to the default position
 *
 */
void alphanumericDisplay_cursorToHome(void) {
    write(CMD_RETURN_HOME, COMMAND);
    HAL_Delay(2);  // 2ms
}

/**
 * @brief Set position cursor
 *
 * @param[in]   row variable type uint8_t, new cursor row
 * @param[in]   column variable type uint8_t, new cursor column
 *
 * @return 0 success, or a negative error code.
 */
alphanumericDisplay_Error alphanumericDisplay_setCursor(uint8_t row, uint8_t column) {
    if (row >= adconf.row) {
        return AD_OOR;
    }
    if (column >= adconf.column) {
        return AD_OOR;
    }
    write(CMD_DISPLAY_RAM_ADDRESS | (col + adconf.rowOffsets[row]), COMMAND);
    return AD_OK;
}

/**
 * @brief Blink cursor
 *
 * @param[in]   enable variable type bool, true - blinking cursor / false - no blinkingg cursor
 *
 */
void alphanumericDisplay_cursorBlinkOn(bool enable) {
    if (enable) {
        write(CMD_CURSOR_MODE_SET | CURSOR_BLINK_ON, COMMAND);
    } else {
        write(CMD_CURSOR_MODE_SET | CURSOR_BLINK_OFF, COMMAND);
    }
}

/**
 * @brief Show cursor
 *
 * @param[in]   enable variable type bool, true - show cursor / false - hide cursor
 *
 */
void alphanumericDisplay_CursorOn(bool enable) {
    if (enable) {
        write(CMD_CURSOR_MODE_SET | CURSOR_ON, COMMAND);
    } else {
        write(CMD_CURSOR_MODE_SET | CURSOR_OFF, COMMAND);
    }
}

/**
 * @brief Display on
 *
 * @param[in]   enable variable type bool, true - on display / false - off display
 *
 */
void alphanumericDisplay_displayOn(bool enable) {
    if (enable) {
        write(CMD_CURSOR_MODE_SET | DISPLAY_ON, COMMAND);
    } else {
        write(CMD_CURSOR_MODE_SET | DISPLAY_OFF, COMMAND);
    }
}

/**
 * @brief Set display scroll direction 
 *
 * @param[in]   dir variable type enum alphanumericDisplay_direction, AD_RIGHT or AD_LEFT
 *
 */
void alphanumericDisplay_displayScroll(enum alphanumericDisplay_direction dir) {
    if (dir) {
        write(CMD_SHIFT_CURSOR | DATA_SHIFT | SHIFT_RIGHT, COMMAND);
    } else {
        write(CMD_SHIFT_CURSOR | DATA_SHIFT | SHIFT_LEFT, COMMAND);
    }
}

/**
 * @brief Set display or cursor auto scroll direction 
 *
 * @param[in]   cd variable type enum alphanumericDisplay_cursorDisplay, selection of what should be auto scroll:
 *                                                                                           AD_CURSOR or AD_DISPLAY
 * @param[in]   dir variable type enum alphanumericDisplay_direction, direction auto scroll: AD_RIGHT or AD_LEFT
 *
 */
void alphanumericDisplay_autoScroll(enum alphanumericDisplay_cursorDisplay cd, enum alphanumericDisplay_direction dir) {
    if (cd) {
        if (dir) {
            write(CMD_DISPLAY_BEHAVIOR | DATA_SHIFT | SHIFT_RIGHT, COMMAND);
        } else {
            write(CMD_DISPLAY_BEHAVIOR | DATA_SHIFT | SHIFT_LEFT, COMMAND);
        }
    } else {
        if (dir) {
            write(CMD_DISPLAY_BEHAVIOR | BEHAVIOR_CURSOR_SHIFT | BEHAVIOR_SHIFT_RIGHT, COMMAND);
        } else {
            write(CMD_DISPLAY_BEHAVIOR | BEHAVIOR_CURSOR_SHIFT | BEHAVIOR_SHIFT_LEFT, COMMAND);
        }
    }
}

/**
 * @brief To send one char
 *        To display your own char use enum alphanumericDisplay_specialChar
 *
 * @param[in]   c variable type char, char to display
 *
 */
void alphanumericDisplay_sendChar(char c) {
    write((uint8_t)c, DATA);
}

/**
 * @brief To send char array
 *
 * @param[in]   c pointer type char, array of char to display
 *
 */
void alphanumericDisplay_sendString(char *c) {
    for (int i = 0; i < strlen(c); i++) {
        write((uint8_t)c[i], DATA);
    }
}

/**
 * @brief saving your own char in the screen memory, max 8 characters
 *
 * @param[in]   sc variable type enum alphanumericDisplay_specialChar, number own char in memory
 * @param[in]   map pointer type uint8_t, array containing 5 columns and 8 rows from 0 and 1, representing its own char
 *
 */
void alphanumericDisplay_saveSpecialChar(enum alphanumericDisplay_specialChar sc, uint8_t *map) {  // max 8 special char
    command(CMD_CHAR_GENERATOR_ADDRESS | sc << 3);
    for (int i = 0; i < 8; i++) {
        write(map[i], DATA);
    }
}

/**
 * @brief Set the contrast of the screen (if used)
 *
 * @param[in]   brightness variable type uint8_t, constarst 0 - 255
 *
 * @return 0 success, or a negative error code.
 */
alphanumericDisplay_Error alphanumericDisplay_setContrast(uint8_t contrast) {
    if (adconf.modeControlContrast) {
        uint32_t var = (uint32_t)(contrast * MAX_12BIT) / SUPPLY_VOLTAGE;

        HAL_DAC_SetValue(adconf.dacV0, adconf.dacChannelV0, DAC_ALIGN_12B_R, var);
        return AD_OK;
    }
    return AD_UNDEFINED;
}

/**
 * @brief Set the brightness of the screen backlight (if used)
 *
 * @param[in]   brightness variable type uint8_t, backlight brightness 0 - 255
 *
 * @return 0 success, or a negative error code.
 */
alphanumericDisplay_Error alphanumericDisplay_setBacklight(uint8_t brightness) {
    switch (adconf.modeBacklight) {
    case BACKLIGHT_PWM:
        if (brightness > 0) {
            adconf.timtimBacklight->Instance->CCRX = brightness;
            HAL_TIM_PWM_Start(adconf.timBacklight, adconf.timChannelBacklight);
        } else {
            adconf.timtimBacklight->Instance->CCRX = 0;
            HAL_TIM_PWM_Stop(adconf.timBacklight, adconf.timChannelBacklight);
        }
        break;
    case BACKLIGHT_IO:
        if (brightness > 0) {
            HAL_GPIO_WritePin(adconf.portBacklight, adconf.pinBacklight, GPIO_PIN_SET);
        } else {
            HAL_GPIO_WritePin(adconf.portBacklight, adconf.pinBacklight, GPIO_PIN_RESET);
        }
        break;
    case BACKLIGHT_OFF:
    default:
        return AD_UNDEFINED;
    }
    return AD_OK;
}

/**
 * @brief Turning on the display power (if used)
 *
 * @param[in]   enable variable type bool, true is a logical high / false is a logical low
 *
 * @return 0 success, or a negative error code.
 */
alphanumericDisplay_Error alphanumericDisplay_powerOn(bool enable) {
    if (adconf.modeControlPower) {
        if (enable) {
            HAL_GPIO_WritePin(adconf.portPower, adconf.pinPower, GPIO_PIN_SET);
        } else {
            HAL_GPIO_WritePin(adconf.portPower, adconf.pinPower, GPIO_PIN_RESET);
        }
        return AD_OK;
    }
    return AD_UNDEFINED;
}

static void setRowOffsets(int row0, int row1, int row2, int row3) {
    adconf.rowOffsets[0] = row0;
    adconf.rowOffsets[1] = row1;
    adconf.rowOffsets[2] = row2;
    adconf.rowOffsets[3] = row3;
}

/*
 * Minimum interface:
 * rs/e/d4/d5/d6/d7
 */
// TODO(Adam): Add support read
// TODO(Adam): Add example!!!

/**
 * @brief Initialization of the alphanumeric LCD display
 *        The driver has additional functionalities enabling support for:
 *          Communication interface with a 4-bit or 8-bit display
 *          Writing and reading support (reading is currently not supported)    (optional)
 *          Ability to manage display power                                     (optional)
 *          Changing the contrast when using DAC                                (optional)
 *          Backlight support in IO or PWM(timer) mode                          (optional)
 *
 * @param[in]   pinPower variable type uint32_t, pin number for display power supply (optional)
 * @param[in]   portPower pointer type GPIO_TypeDef, port for powering the display (NULL if unused)
 *
 * @param[in]   dacV0 pointer type DAC_HandleTypeDef, display contrast management (NULL if unused)
 * @param[in]   dacChannelV0 variable type uint32_t, DAC channel no. to be used to control display contrast (optional)
 *
 * @param[in]   pinRS variable type uint32_t, pin number for selecting data type
 * @param[in]   portRS pointer type GPIO_TypeDef, port for selecting data type
 *
 * @param[in]   pinRW variable type uint32_t, pin number for selecting write or read (optional)
 * @param[in]   portRW pointer type GPIO_TypeDef, port for selecting write or read (NULL if unused)
 *
 * @param[in]   pinE variable type uint32_t, pin number for update display
 * @param[in]   portE pointer type GPIO_TypeDef, port for update display
 *
 * @param[in]   pinD0 variable type uint32_t, pin number for the 8 bit interface (optional)
 * @param[in]   portD0 pointer type GPIO_TypeDef, port for the 8 bit interface (NULL if used 4 bit)
 * @param[in]   pinD1 variable type uint32_t, pin number for the 8 bit interface (optional)
 * @param[in]   portD1 pointer type GPIO_TypeDef, port for the 8 bit interface (NULL if used 4 bit)
 * @param[in]   pinD2 variable type uint32_t, pin number for the 8 bit interface (optional)
 * @param[in]   portD2 pointer type GPIO_TypeDef, port for the 8 bit interface (NULL if used 4 bit)
 * @param[in]   pinD3 variable type uint32_t, pin number for the 8 bit interface (optional)
 * @param[in]   portD3 pointer type GPIO_TypeDef, port for the 8 bit interface (NULL if used 4 bit)
 *
 * @param[in]   pinD4 variable type uint32_t, pin number for the 8 bit and 4 bit interface
 * @param[in]   portD4 pointer type GPIO_TypeDef, port for the 8 bit and 4 bit interface
 * @param[in]   pinD5 variable type uint32_t, pin number for the 8 bit and 4 bit interface
 * @param[in]   portD5 pointer type GPIO_TypeDef, port for the 8 bit and 4 bit interface
 * @param[in]   pinD6 variable type uint32_t, pin number for the 8 bit and 4 bit interface
 * @param[in]   portD6 pointer type GPIO_TypeDef, port for the 8 bit and 4 bit interface
 * @param[in]   pinD7 variable type uint32_t, pin number for the 8 bit and 4 bit interface
 * @param[in]   portD7 pointer type GPIO_TypeDef, port for the 8 bit and 4 bit interface
 *
 * @param[in]   pinBacklight variable type uint32_t, pin number for backlight IO (optional)
 * @param[in]   portBacklight pointer type GPIO_TypeDef, port for backlight IO (NULL if used pwm or unused)
 *
 * @param[in]   timBacklight pointer type TIM_HandleTypeDef, display backlight management (NULL if unused)
 * @param[in]   timChannelBacklight variable type uint32_t, timer channel number to be used to
 *              control display backlight (optional)
 *
 * @param[in]   row variable type uint8_t, number of row in the display (range 1 - 4)
 * @param[in]   column variable type uint8_t, number of columns in the display (range 1 - 20)
 *
 * @return 0 success, or a negative error code.
 */
alphanumericDisplay_Error alphanumericDisplay_init(uint32_t pinPower, GPIO_TypeDef *portPower,           // (optional)
                                        DAC_HandleTypeDef *dacV0, uint32_t dacChannelV0,                 // (optional)
                                        uint32_t pinRS, GPIO_TypeDef *portRS,
                                        uint32_t pinRW, GPIO_TypeDef *portRW,                            // (optional)
                                        uint32_t pinE, GPIO_TypeDef *portE,
                                        uint32_t pinD0, GPIO_TypeDef *portD0,                            // (optional)
                                        uint32_t pinD1, GPIO_TypeDef *portD1,                            // (optional)
                                        uint32_t pinD2, GPIO_TypeDef *portD2,                            // (optional)
                                        uint32_t pinD3, GPIO_TypeDef *portD3,                            // (optional)
                                        uint32_t pinD4, GPIO_TypeDef *portD4,                            // (optional)
                                        uint32_t pinD5, GPIO_TypeDef *portD5,
                                        uint32_t pinD6, GPIO_TypeDef *portD6,
                                        uint32_t pinD7, GPIO_TypeDef *portD7,
                                        uint32_t pinBacklight, GPIO_TypeDef *portBacklight,              // (optional)
                                        TIM_HandleTypeDef *timBacklight, uint32_t timChannelBacklight,   // (optional)
                                        uint8_t row, uint8_t column) {

    if ((portRS == NULL) || (portE == NULL) || (portD4 == NULL) ||
        (portD5 == NULL) || (portD6 == NULL) || (portD7 == NULL)) {
        return AD_UNSUPPORTED;
    }

    if ((row > MAX_SUPPORT_ROW) || (column > MAX_SUPPORT_COLUMN) || (row < 1) || (column < 1)) {
        return AD_UNSUPPORTED;
    }

    adconf.pinPower = pinPower;
    adconf.portPower = portPower;
    adconf.dacV0 = dacV0;
    adconf.dacChannelV0 = dacChannelV0;
    adconf.pinRS = pinRS;
    adconf.portRS = portRS;
    adconf.pinRW = pinRW;
    adconf.portRW = portRW;
    adconf.pinE = pinE;
    adconf.portE = portE;
    adconf.pinD0 = pinD0;
    adconf.portD0 = portD0;
    adconf.pinD1 = pinD1;
    adconf.portD1 = portD1;
    adconf.pinD2 = pinD2;
    adconf.portD2 = portD2;
    adconf.pinD3 = pinD3;
    adconf.portD3 = portD3;
    adconf.pinD4 = pinD4;
    adconf.portD4 = portD4;
    adconf.pinD5 = pinD5;
    adconf.portD5 = portD5;
    adconf.pinD6 = pinD6;
    adconf.portD6 = portD6;
    adconf.pinD7 = pinD7;
    adconf.portD7 = portD7;
    adconf.pinBacklight = pinBacklight;
    adconf.portBacklight = portBacklight;
    adconf.timBacklight = timBacklight;
    adconf.timChannelBacklight = timChannelBacklight;
    adconf.row = row;
    adconf.column = column;

    if ((portD0 != NULL) && (portD1 != NULL) && (portD2 != NULL) && (portD3 != NULL)) {
        adconf.mode8bit = true;
    }

    if (portRW != NULL) {
        adconf.modeRwOff = true;
        HAL_GPIO_WritePin(adconf.portRW, adconf.pinRW, GPIO_PIN_RESET);
    }

    if (portPower != NULL) {
        adconf.modeControlPower = true;
        HAL_GPIO_WritePin(adconf.portPower, adconf.pinPower, GPIO_PIN_SET);
    }

    if (dacV0 != NULL) {
        adconf.modeControlContrast = true;
    }

    if (timBacklight != NULL) {
        adconf.modeBacklight = BACKLIGHT_PWM;
        adconf.timtimBacklight->Instance->PSC = 0;
        adconf.timtimBacklight->Instance->ARR = (HAL_RCC_GetSysClockFreq() / BACKLIGHT_LIGHT_PRECISION) - 1;
    } else {
        if (portBacklight != NULL) {
            adconf.modeBacklight = BACKLIGHT_IO;
            HAL_GPIO_WritePin(adconf.portBacklight, adconf.pinBacklight, GPIO_PIN_RESET);
        }
    }

    setRowOffsets(0x00, 0x40, 0x00 + coolumn, 0x40 + coolumn);

    HAL_GPIO_WritePin(adconf.portE, adconf.pinE, GPIO_PIN_RESET);
    
    uint8_t config = 0;

    if (adconf.mode8bit = false) {
        config |= INTERFACE_4BIT;
    }
    if (adconf.row > 1) {
        config |= DISPLAY_TWO_LINE;
    }

    write(CMD_CONFIGURATION_SET | config, COMMAND);

    return AD_OK;
 }

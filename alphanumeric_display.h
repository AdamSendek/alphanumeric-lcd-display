/*
 * Copyright (C) 2024 Adam Sendek
 *
 * These computer program listings and specifications,
 * are property of Adam Sendek and are licensed under
 * Creative Commons Attribution-NonCommercial-ShareAlike
 * 4.0 International Public License
 */

#ifndef ALPHANUMERIC_DISPLAY_H_
#define ALPHANUMERIC_DISPLAY_H_

#define CCRX                        CCR1
#define SUPPLY_VOLTAGE              (float)3.3

enum alphanumericDisplay_Error {
    AD_OK = 0,
    AD_IO,
    AD_UNDEFINED,
    AD_UNSUPPORTED,
    AD_OOR,  // Out of Range
};

enum alphanumericDisplay_specialChar {
    AD_SC0 = 0,
    AD_SC1,
    AD_SC2,
    AD_SC3,
    AD_SC4,
    AD_SC5,
    AD_SC6,
    AD_SC7,
};

enum alphanumericDisplay_cursorDisplay {
    AD_CURSOR = 0,
    AD_DISPLAY,
};


enum alphanumericDisplay_direction {
    AD_LEFT = 0,
    AD_RIGHT,
};


void alphanumericDisplay_clear(void);
void alphanumericDisplay_cursorToHome(void);
alphanumericDisplay_Error alphanumericDisplay_setCursor(uint8_t row, uint8_t column);
void alphanumericDisplay_cursorBlinkOn(bool enable);
void alphanumericDisplay_CursorOn(bool enable);
void alphanumericDisplay_displayOn(bool enable);
void alphanumericDisplay_displayScroll(bool direction);
void alphanumericDisplay_autoScroll(bool displayCursor, bool direction);
void alphanumericDisplay_cursorAutoScroll(bool direction);
void alphanumericDisplay_sendChar(char c);  // To write special char use enum specialChar
void alphanumericDisplay_sendString(char *c);
void alphanumericDisplay_saveSpecialChar(enum specialChar sc, uint8_t *map);  // only 8 special char
alphanumericDisplay_Error alphanumericDisplay_setContrast(uint8_t contrast);
alphanumericDisplay_Error alphanumericDisplay_setBacklight(uint8_t brightness);
void alphanumericDisplay_powerOn(bool enable);
alphanumericDisplay_Error alphanumericDisplay_init(uint32_t pinPower, GPIO_TypeDef *portPower,           // (optional)
                                        DAC_HandleTypeDef *dacV0, uint32_t dacChannelV0,                 // (optional)
                                        uint32_t pinRS, GPIO_TypeDef *portRS,
                                        uint32_t pinRW, GPIO_TypeDef *portRW,                            // (optional)
                                        uint32_t pinE, GPIO_TypeDef *portE,
                                        uint32_t pinD0, GPIO_TypeDef *portD0,                            // (optional)
                                        uint32_t pinD1, GPIO_TypeDef *portD1,                            // (optional)
                                        uint32_t pinD2, GPIO_TypeDef *portD2,                            // (optional)
                                        uint32_t pinD3, GPIO_TypeDef *portD3,                            // (optional)
                                        uint32_t pinD4, GPIO_TypeDef *portD4,
                                        uint32_t pinD5, GPIO_TypeDef *portD5,
                                        uint32_t pinD6, GPIO_TypeDef *portD6,
                                        uint32_t pinD7, GPIO_TypeDef *portD7,
                                        uint32_t pinBacklight, GPIO_TypeDef *portBacklight,              // (optional)
                                        TIM_HandleTypeDef *timBacklight, uint32_t timChannelBacklight,   // (optional)
                                        uint8_t row, uint8_t column);

#endif  // ALPHANUMERIC_DISPLAY_H_
